#pragma once
#ifndef _JOSH_CURL_MULTI_WRAP_H_
#define _JOSH_CURL_MULTI_WRAP_H_

#include <memory>
#include <iosfwd>
#include <string>
#include <vector>
#include <chrono>
#include <ctime>


namespace josh {

class HeaderBuilder;

/**Wrap class of libcurl multi-interface.
 This class provides the following abilities:
   -
 */
class CurlMultiWrap {
public:
    /**Defines CURL debug info type.*/
    enum DebugInfoType {
        Text = 0,   //! CURL information text
        HeaderIn,   //! received header data
        HeaderOut,  //! header data to be sent
        DataIn,     //! received request data
        DataOut,    //! request data to be sent
        SSLIn,      //! received SSL binary data 
        SSLOut      //! SSL binary response data
    };

private:
    struct Impl;
    std::unique_ptr<Impl> impl;
};

} // namespace josh

#endif // ! _JOSH_CURL_MULTI_WRAP_H_

