#ifdef _MSC_VER
#define NOMINMAX
#endif
#include "curlwrap.hpp"
#include "headerbuilder.hpp"
#include "impl/curlwrapimpl.hpp"
#include "impl/headerbuilderimpl.hpp"
#include <curl/curl.h>
#include <utility>
#include <map>
#include <fstream>
#include <filesystem>
#include <algorithm>
#include <iostream>

#ifdef _WIN32 
#include <winsock.h>    // timeval
#include <WinSock2.h>   // select
#else
#include <unistd.h>
#include <sys/time.h>
#endif

using namespace josh;

namespace {

constexpr char const* MethodToString(CurlWrap::Method method) {
    constexpr char const* Table[] = { "GET", "POST", "PUT", "DELETE", "OPTIONS" };
    return Table[static_cast<int>(method)];
}

}


/*************************************************************/
// CurlWrap
/*************************************************************/
bool CurlWrap::initialize() { return !curl_global_init(CURL_GLOBAL_DEFAULT); }


void CurlWrap::cleanUp() { curl_global_cleanup(); }


long long CurlWrap::querySize(CurlWrap* curlWrap, std::string const& url) {
    std::unique_ptr<CurlWrap> curl;
    if (!curlWrap) {
        curl = std::make_unique<CurlWrap>();
        curlWrap = curl.get();
    }

    curl_easy_setopt(curlWrap->impl->curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curlWrap->impl->curl, CURLOPT_NOBODY, 1);
    curl_easy_setopt(curlWrap->impl->curl, CURLOPT_FOLLOWLOCATION, 1);

    auto result = curl_easy_perform(curlWrap->impl->curl);

    if (result != CURLE_OK) {
        return -1;
    }

    long long size = -1;
    curl_easy_getinfo(curlWrap->impl->curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD_T, &size);
    return size;
}


bool CurlWrap::download(
    std::vector<CurlWrap>* curlWrapList, std::string const& url, std::string const& path, int parallel
) {
    static constexpr timeval DefaultIOTime = { 1, 0 };
    if (parallel <= 0)  return false;
    auto const filePath = std::filesystem::u8path(path);

    std::vector<CurlWrap> list;
    if (!curlWrapList) curlWrapList = &list;
    if (parallel > static_cast<int>(curlWrapList->size()))
        curlWrapList->resize(parallel);

    auto& head = curlWrapList->operator[](0);
    head.enableRedirect()
        .enableSSLVerify()
        .addCookiesFromFile("");
    // Query size
    auto const predictSize = querySize(&head, url);

    // Cancel download if request is directly failed
    //if (!head.isSuccess()) return false;

    // Reserve target stream size
    if (predictSize > 0) {
        // Create file if not exist
        std::error_code code;
        if (!std::filesystem::is_regular_file(filePath, code))
            std::ofstream{ filePath }.put('a');
        std::filesystem::resize_file(filePath, predictSize);
    }
    else
        // Cannot get real size, download by single connection
        parallel = 1;
    auto const dataRange = predictSize / parallel;

    // Open file
    std::ofstream out(filePath, std::ios::binary | std::ios::ate | std::ios::out | std::ios::in);
    out.seekp(0);

    // Prepare data writer callback parameter
    struct Param {
        std::ostream* out;
        size_t start, end, current;
        int index;
    };
    auto paramList = std::make_unique<Param[]>(parallel);

    // Create multi CURL head
    auto* multiCurl = curl_multi_init();
    for (auto i = 0; i < parallel; ++i) {
        auto& curlWrap = curlWrapList->operator[](i);
        curlWrap.setUrl(url)
            .setMethod(Method::GET)
            .enableRedirect()
            .enableSSLVerify()
            .addCookiesFromFile("");

        paramList[i].out = &out;
        if (predictSize > 0) {
            auto const start = i * dataRange;
            auto const end = i != (parallel -1) ? start + dataRange - 1 : predictSize - 1;
            curlWrap.setHeader(std::move(HeaderBuilder().setRange(start, end)));
            paramList[i].start = start;
            paramList[i].end = end;
            paramList[i].current = start;
            paramList[i].index = i;
        }
        curlWrap.setWriteDataCallback(
            [](char const* data, size_t, size_t size, void* param) {
                auto* writer = static_cast<Param*>(param);
                writer->out->seekp(writer->current);
                auto const length = std::min(size, writer->end - writer->current + 1);
                writer->out->write(data, length);
                writer->current += length;
                return size;
            }, &(paramList[i]));

        curl_multi_add_handle(multiCurl, curlWrap.impl->curl);
    }

    ;
    // nonblocking download, see https://curl.haxx.se/libcurl/c/multi-app.html
    auto remainConnection = 0;
    curl_multi_perform(multiCurl, &remainConnection);
    while (remainConnection) {
        fd_set readSet, writeSet, exceptionSet;
        FD_ZERO(&readSet);
        FD_ZERO(&writeSet);
        FD_ZERO(&exceptionSet);

        // Query CURL non-blocking timeout
        auto ioTimeout = DefaultIOTime;
        long curlTimeout = -1;
        curl_multi_timeout(multiCurl, &curlTimeout);
        if (curlTimeout >= 0) {
            ioTimeout.tv_sec = curlTimeout / 1000;
            if (ioTimeout.tv_sec > 1)
                ioTimeout.tv_sec = 1;
            else
                ioTimeout.tv_usec = (curlTimeout % 1000) * 1000;
        }

        // select
        auto maxfd = -1;
        auto const error = curl_multi_fdset(multiCurl, &readSet, &writeSet, &exceptionSet, &maxfd);
        if (error != CURLM_OK) {
            // There's error on non-blocking IO
            break;
        }


        int selectResult;
        if (maxfd == -1) {
            // No ready fd, wait for 100ms and try again
#ifdef _WIN32
            Sleep(100);
            selectResult = 0;
#else
            /* Portable sleep for platforms other than Windows. */
            timeval wait = { 0, 100 * 1000 }; /* 100ms */
            selectResult = select(0, NULL, NULL, NULL, &wait);
#endif
        }
        else {
            /* Note that on some platforms 'timeout' may be modified by select().
               If you need access to the original value save a copy beforehand. */
            selectResult = select(maxfd + 1, &readSet, &writeSet, &exceptionSet, &ioTimeout);
        }

        if (selectResult >= 0)
            curl_multi_perform(multiCurl, &remainConnection);
        else {
            // select error, skip and try again
        }
    }

    CURLMsg* msg;
    int remainMsg;
    std::vector<int> resultList(parallel);
    while ((msg = curl_multi_info_read(multiCurl, &remainMsg))) {
        if (msg->msg == CURLMSG_DONE) {
            int idx;

            /* Find out which handle this message is about */
            for (idx = 0; idx < parallel; idx++) {
                auto* curl = curlWrapList->operator[](idx).impl->curl;
                if (msg->easy_handle == curl) break;
            }

            resultList[idx] = msg->data.result;
        }
    }
    ;
    // Try download remaining if still not complete
    for (auto i = 0; i < parallel; ++i) {
        std::cout << "Parallel state #" << i << ": " << curl_easy_strerror(static_cast<CURLcode>(resultList[i])) << std::endl;
        auto& param = paramList[i];
        std::cout << "   Hit end: " << (param.current == param.end + 1) << std::endl;
        std::cout << "   Current: " << param.current << ", end: " << param.end << std::endl;
    }

    ;
    curl_multi_cleanup(multiCurl);
    /// TODO
    return false;
}


bool CurlWrap::download(
    std::vector<CurlWrap>* curlWrapList, std::string const& url, std::ostream& out,
    int parallel
) {
    /// TODO
    return false;
}


CurlWrap::CurlWrap(): impl(new Impl) {
    impl->curl = curl_easy_init();
}


CurlWrap::~CurlWrap() noexcept = default;


CurlWrap::CurlWrap(CurlWrap const& o): impl(new Impl) {
    impl->curl = curl_easy_duphandle(o.impl->curl);
}


CurlWrap::CurlWrap(CurlWrap&& o) noexcept: impl(std::move(o.impl)) {}


CurlWrap& CurlWrap::operator=(CurlWrap&& o) noexcept {
    impl = std::move(o.impl);
    return *this;
}


CurlWrap& CurlWrap::operator=(CurlWrap const& o) {
    impl->curl = curl_easy_duphandle(o.impl->curl);
    return *this;
}


CurlWrap& CurlWrap::setUrl(std::string const& url) {
    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_URL, url.c_str()));
    return *this;
}


CurlWrap& CurlWrap::setMethod(Method method) {
    switch (method) {
    case Method::GET:
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_HTTPGET, 1L));
        break;
    case Method::POST:
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_HTTPPOST, 1L));
        break;
    case Method::PUT:
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_UPLOAD, 1L));
        break;
    default:
        return setMethod(MethodToString(method));
    }
    return *this;
}


CurlWrap& CurlWrap::setMethod(std::string const& method) {
    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_CUSTOMREQUEST, method.c_str()));
    return *this;
}


CurlWrap& CurlWrap::setHeader(HeaderBuilder&& headerBuilder) {
    impl->header = std::move(headerBuilder);
    auto const* headerImpl = impl->header.impl.get();

    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_HTTPHEADER, headerImpl->headerList));   
    // Set keep-alive relative options
    if (headerImpl->isKeepAlive) {
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_TCP_KEEPALIVE, 1));
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_TCP_KEEPIDLE, headerImpl->keepAliveIdleTime));
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_TCP_KEEPINTVL, headerImpl->keepAliveProbeTime));
    }
    else 
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_TCP_KEEPALIVE, 0));
    // Set Range
    if (headerImpl->range.empty())
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_RANGE, nullptr));
    else
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_RANGE, headerImpl->range.c_str()));
    // User-Agent
    if (!headerImpl->userAgent.empty())
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_USERAGENT, headerImpl->userAgent.c_str()));
    // Referer
    if (!headerImpl->referer.empty())
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_REFERER, headerImpl->referer.c_str()));
    // Accept-Encoding
    if (!headerImpl->acceptEncoding.empty())
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_ACCEPT_ENCODING, headerImpl->acceptEncoding.c_str()));
    // Cookie
    if (!headerImpl->cookie.empty())
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_COOKIE, headerImpl->cookie.c_str()));
    return *this;
}


CurlWrap& CurlWrap::setHeader(HeaderBuilder const& headerBuilder) {
    impl->header = headerBuilder;
    auto const* headerImpl = impl->header.impl.get();

    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_HTTPHEADER, headerImpl->headerList));
    // Set keep-alive relative options
    if (headerImpl->isKeepAlive) {
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_TCP_KEEPALIVE, 1));
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_TCP_KEEPIDLE, headerImpl->keepAliveIdleTime));
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_TCP_KEEPINTVL, headerImpl->keepAliveProbeTime));
    }
    else 
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_TCP_KEEPALIVE, 0));
    // Set Range
    if (headerImpl->range.empty())
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_RANGE, nullptr));
    else
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_RANGE, headerImpl->range.c_str()));
    // User-Agent
    if (!headerImpl->userAgent.empty())
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_USERAGENT, headerImpl->userAgent.c_str()));
    // Referer
    if (!headerImpl->referer.empty())
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_REFERER, headerImpl->referer.c_str()));
    // Accept-Encoding
    if (!headerImpl->acceptEncoding.empty())
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_ACCEPT_ENCODING, headerImpl->acceptEncoding.c_str()));
    // Cookie
    if (!headerImpl->cookie.empty())
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_COOKIE, headerImpl->cookie.c_str()));
    return *this;
}


CurlWrap& CurlWrap::clearHeader() {
    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_HTTPHEADER, nullptr));
    return *this;
}


CurlWrap& CurlWrap::setPostBody(char const* rawData, size_t* realWriteSize, long long size, bool copy) {
    if (copy) {
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_POSTFIELDSIZE_LARGE, size >= 0 ? size: -1));
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_COPYPOSTFIELDS, rawData));
    }
    else {
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_POSTFIELDSIZE_LARGE, size >= 0 ? size: -1));
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_POSTFIELDS, rawData));
    }
    if (realWriteSize) *realWriteSize = size;
    return *this;
}


CurlWrap& CurlWrap::addGetParameter(std::string const& key, std::string const& value) {
    impl->getParameters[key] = value;
    return *this;
}


CurlWrap& CurlWrap::setWriteDataCallback(WriteDataCallback callback, void* customParameter) {
    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_WRITEFUNCTION, callback));
    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_WRITEDATA, customParameter));
    return *this;
}


CurlWrap& CurlWrap::setHeaderDataCallback(WriteDataCallback callback, void* customParameter) {
    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_HEADERFUNCTION, callback));
    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_HEADERDATA, customParameter));
    return *this;
}


CurlWrap& CurlWrap::setReadDataCallback(ReadDataCallback callback, void* customParameter) {
    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_READFUNCTION, callback));
    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_READDATA, customParameter));
    return *this;
}


CurlWrap& CurlWrap::perform() {
    impl->setResult(curl_easy_perform(impl->curl));
    return *this;
}


void CurlWrap::reset() {
    curl_easy_reset(impl->curl);
}


CurlWrap& CurlWrap::addCookiesFromFile(std::string const& cookieFilePath) {
    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_COOKIEFILE, cookieFilePath.c_str()));
    return *this;
}


CurlWrap& CurlWrap::addCookies(std::string const& rawCookieString) {
    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_COOKIELIST, rawCookieString.c_str()));
    return *this;
}


CurlWrap& CurlWrap::newCookieSession() {
    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_COOKIESESSION, 1));
    return *this;
}


void CurlWrap::flushCookieToFile(std::string const& cookieFilePath) {
    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_COOKIEJAR, cookieFilePath.c_str()));
    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_COOKIELIST, "FLUSH"));
    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_COOKIEJAR, nullptr));
}


void CurlWrap::clearCookies() {
    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_COOKIELIST, "ALL"));
}


void CurlWrap::reloadCookies() {
    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_COOKIELIST, "RELOAD"));
}


CurlWrap& CurlWrap::setTimeout(int ms) {
    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_TIMEOUT_MS, ms));
    return *this;
}


CurlWrap& CurlWrap::setConnectionTimeout(int ms) {
    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_CONNECTTIMEOUT_MS, ms));
    return *this;
}


CurlWrap& CurlWrap::setCancelSpeed(size_t bytesPerSecond, int seconds) {
    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_LOW_SPEED_LIMIT, bytesPerSecond));
    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_LOW_SPEED_TIME, seconds));
    return *this;
}


CurlWrap& CurlWrap::setUploadSize(size_t sizeInByte) {
    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_POSTFIELDSIZE_LARGE, sizeInByte));
    return *this;
}


CurlWrap& CurlWrap::setUploadSpeedLimit(size_t bytesPerSecond) {
    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_MAX_SEND_SPEED_LARGE, bytesPerSecond));
    return *this;
}


CurlWrap& CurlWrap::setDownloadSpeedLimit(size_t bytesPerSecond) {
    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_MAX_RECV_SPEED_LARGE, bytesPerSecond));
    return *this;
}


CurlWrap& CurlWrap::setMaxConnection(long connections) {
    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_MAXCONNECTS, connections));
    return *this;
}


CurlWrap& CurlWrap::enableSSLVerify(bool enable, bool useSystemCA) {
    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_SSL_VERIFYPEER, enable ? 1: 0));
    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_SSL_OPTIONS, 
        useSystemCA ? CURLSSLOPT_NATIVE_CA: 0));
    return *this;
}


CurlWrap& CurlWrap::enableRedirect(bool enable) {
    impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_FOLLOWLOCATION, enable ? 1: 0));
    return *this;
}

CurlWrap& CurlWrap::setCustomOption(unsigned curlOption, long value) {
    impl->setResult(curl_easy_setopt(impl->curl, static_cast<CURLoption>(curlOption), value));
    return *this;
}


CurlWrap& CurlWrap::setCustomOption(unsigned curlOption, char const* value) {
    impl->setResult(curl_easy_setopt(impl->curl, static_cast<CURLoption>(curlOption), value));
    return *this;
}


CurlWrap& CurlWrap::setCustomOption(unsigned curlOption, void* value) {
    impl->setResult(curl_easy_setopt(impl->curl, static_cast<CURLoption>(curlOption), value));
    return *this;
}


std::string CurlWrap::getErrorMessage() const { return impl->msg; }


bool CurlWrap::isSuccess() const { return impl->result; }


int CurlWrap::getErrorCode() const { return impl->rawCode; }


int CurlWrap::getResponseCode() const {
    long code;
    impl->setResult(curl_easy_getinfo(impl->curl, CURLINFO_RESPONSE_CODE, &code));
    return static_cast<int>(code);
}


std::string CurlWrap::getHttpMethod() const {
    char* method;
    impl->setResult(curl_easy_getinfo(impl->curl, CURLINFO_EFFECTIVE_METHOD, &method));
    return method;
}


std::string CurlWrap::getUrl() const {
    char* url;
    impl->setResult(curl_easy_getinfo(impl->curl, CURLINFO_EFFECTIVE_URL, &url));
    return url;
}


std::string CurlWrap::getRemoteIP() const {
    char* ip;
    impl->setResult(curl_easy_getinfo(impl->curl, CURLINFO_PRIMARY_IP, &ip));
    return ip;
}


int CurlWrap::getRemotePort() const {
    long port;
    impl->setResult(curl_easy_getinfo(impl->curl, CURLINFO_PRIMARY_PORT, &port));
    return static_cast<int>(port);
}


std::string CurlWrap::getLocalIP() const {
    char* ip;
    impl->setResult(curl_easy_getinfo(impl->curl, CURLINFO_LOCAL_IP, &ip));
    return ip;
}


int CurlWrap::getLocalPort() const {
    long port;
    impl->setResult(curl_easy_getinfo(impl->curl, CURLINFO_LOCAL_PORT, &port));
    return static_cast<int>(port);
}


std::string CurlWrap::getHttpContentType() const {
    char* contentType;
    impl->setResult(curl_easy_getinfo(impl->curl, CURLINFO_CONTENT_TYPE, &contentType));
    return contentType;
}


long long CurlWrap::getHttpRetryAfterTime() const {
    curl_off_t retryAfter = 0;
    impl->setResult(curl_easy_getinfo(impl->curl, CURLINFO_RETRY_AFTER, &retryAfter));
    return retryAfter;
}


std::time_t CurlWrap::getFileTime() const {
    curl_off_t fileTime;
    impl->setResult(curl_easy_getinfo(impl->curl, CURLINFO_FILETIME_T, &fileTime));
    return fileTime;
}


size_t CurlWrap::getUsedConnectionCount() const {
    long connections = 0;
    impl->setResult(curl_easy_getinfo(impl->curl, CURLINFO_NUM_CONNECTS, &connections));
    return connections;
}


std::vector<std::string> CurlWrap::getHttpCookies() const {
    std::vector<std::string> cookies;
    curl_slist* head = nullptr;
    impl->setResult(curl_easy_getinfo(impl->curl, CURLINFO_COOKIELIST, &head));
    for (curl_slist* h = head; h; h = h->next) 
        cookies.emplace_back(h->data);
    curl_slist_free_all(head);
    return cookies;
}


int CurlWrap::getRedirectCount() const {
    long count = 0;
    impl->setResult(curl_easy_getinfo(impl->curl, CURLINFO_REDIRECT_COUNT, &count));
    return count;
}


size_t CurlWrap::getHeaderBytes() const {
    long size;
    impl->setResult(curl_easy_getinfo(impl->curl, CURLINFO_HEADER_SIZE, &size));
    return size;
}


size_t CurlWrap::getTotalUploadBytes() const {
    curl_off_t size;
    impl->setResult(curl_easy_getinfo(impl->curl, CURLINFO_SIZE_UPLOAD_T, &size));
    return size;
}


size_t CurlWrap::getTotalDownloadBytes() const {
    curl_off_t size;
    impl->setResult(curl_easy_getinfo(impl->curl, CURLINFO_SIZE_DOWNLOAD_T, &size));
    return size;
}


size_t CurlWrap::getHttpDownloadContentLength() const {
    curl_off_t size;
    impl->setResult(curl_easy_getinfo(impl->curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD_T, &size));
    return size;
}


size_t CurlWrap::getHttpUploadContentLength() const {
    curl_off_t size;
    impl->setResult(curl_easy_getinfo(impl->curl, CURLINFO_CONTENT_LENGTH_UPLOAD_T, &size));
    return size;
}


size_t CurlWrap::getUploadSpeed() const {
    curl_off_t speed;
    impl->setResult(curl_easy_getinfo(impl->curl, CURLINFO_SPEED_UPLOAD_T, &speed));
    return speed;
}


size_t CurlWrap::getDownloadSpeed() const {
    curl_off_t speed;
    impl->setResult(curl_easy_getinfo(impl->curl, CURLINFO_SPEED_DOWNLOAD_T, &speed));
    return speed;
}


std::chrono::microseconds CurlWrap::getTotalTime() const {
    curl_off_t time;
    impl->setResult(curl_easy_getinfo(impl->curl, CURLINFO_TOTAL_TIME_T, &time));
    return std::chrono::microseconds(time);
}


std::chrono::microseconds CurlWrap::getTotalRedirectionTime() const {
    curl_off_t time;
    impl->setResult(curl_easy_getinfo(impl->curl, CURLINFO_REDIRECT_TIME_T, &time));
    return std::chrono::microseconds(time);
}


std::chrono::microseconds CurlWrap::getNameLookupTime() const {
    curl_off_t time;
    impl->setResult(curl_easy_getinfo(impl->curl, CURLINFO_NAMELOOKUP_TIME_T, &time));
    return std::chrono::microseconds(time);
}


std::chrono::microseconds CurlWrap::getConnectElapseTime() const {
    return getConnectTimePoint() - getNameLookupTime();
}


std::chrono::microseconds CurlWrap::getSSLElapseTime() const {
    return getSSLTimePoint() - getConnectTimePoint();
}


std::chrono::microseconds CurlWrap::getPreTransferElapseTime() const {
    return getPreTransferTimePoint() - getSSLTimePoint();
}


std::chrono::microseconds CurlWrap::getStartTransferElapseTime() const {
    return getStartTransferTimePoint() - getPreTransferTimePoint();
}


std::chrono::microseconds CurlWrap::getConnectTimePoint() const {
    curl_off_t time;
    impl->setResult(curl_easy_getinfo(impl->curl, CURLINFO_CONNECT_TIME_T, &time));
    return std::chrono::microseconds(time);
}


std::chrono::microseconds CurlWrap::getSSLTimePoint() const {
    curl_off_t time;
    impl->setResult(curl_easy_getinfo(impl->curl, CURLINFO_APPCONNECT_TIME_T, &time));
    return std::chrono::microseconds(time);
}


std::chrono::microseconds CurlWrap::getPreTransferTimePoint() const {
    curl_off_t time;
    impl->setResult(curl_easy_getinfo(impl->curl, CURLINFO_PRETRANSFER_TIME_T, &time));
    return std::chrono::microseconds(time);
}


std::chrono::microseconds CurlWrap::getStartTransferTimePoint() const {
    curl_off_t time;
    impl->setResult(curl_easy_getinfo(impl->curl, CURLINFO_STARTTRANSFER_TIME_T, &time));
    return std::chrono::microseconds(time);
}


void CurlWrap::enableDebugInfo(DebugCallback callback, void* customParameter) {
    if (callback) {
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_VERBOSE, 1));
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_DEBUGDATA, customParameter));
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_DEBUGFUNCTION, callback));
    }
    else {
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_VERBOSE, 0));
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_DEBUGFUNCTION, nullptr));
        impl->setResult(curl_easy_setopt(impl->curl, CURLOPT_DEBUGDATA, nullptr));
    }
}

