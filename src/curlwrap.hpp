#pragma once
#ifndef _JOSH_CURL_WRAP_H_
#define _JOSH_CURL_WRAP_H_

#include <memory>
#include <iosfwd>
#include <string>
#include <vector>
#include <chrono>
#include <ctime>


namespace josh {

class HeaderBuilder;

/**Wrap class of libcurl.
 This class provides the following abilities:
   -
 */
class CurlWrap {
public:
    /**Defines HTTP request method.*/
    enum class Method {
        GET, POST, PUT, DELETE, OPTIONS,
    };

    /**Defines CURL debug info type.*/
    enum DebugInfoType {
        Text = 0,   //! CURL information text
        HeaderIn,   //! received header data
        HeaderOut,  //! header data to be sent
        DataIn,     //! received request data
        DataOut,    //! request data to be sent
        SSLIn,      //! received SSL binary data 
        SSLOut      //! SSL binary response data
    };

    /**Callback for handling receiving data block.
     @param[in] data received data block. DO NOT ASSUME THIS IS NULL-TERMINATED.
     @param[in] size size in byte of received data block. Note that this value may be 0 if server
        response empty data.
     @param[in] customParameter custom extra parameter.
     @return callback function must return actual processed data size in byte. If this value is
        different from @p size, CURL will raise error and abort data transfer. */
    typedef size_t (*WriteDataCallback)(char const* data, size_t, size_t size, void* customParameter);
    /**Callback for handling sending data block.
     @param[in] data target write buffer. Write data to upload here.
     @param[in] size, item maximum (size * item) bytes to write to @p data.
     @param[in] customParameter custom extra parameter.
     @return callback function must return actual processed data size in byte. Return 0 means
        end-of-file. Return CURL_READFUNC_ABORT to raise error and abort transfer. */
    typedef size_t (*ReadDataCallback)(char* data, size_t size, size_t item, void* customParameter);
    /**Custom debug output callback.
     @note the first parameter is reserved.
     @param[in] type information type.
     @param[in] data raw data.
     @param[in] size size in byte of data block.
     @param[in] customParameter custom extra parameter. */
    typedef void (*DebugCallback)(void*, DebugInfoType type, char const* data, size_t size, void* customParameter);

    /**Initialize global resources.
     This method MUST be called before all other CurlWrap functions.
    @note this method internally calls `curl_global_init(GURL_GLOBAL_ALL)`.
    @see cleanUp()
     */
    static bool initialize();
    /**Deinitialize global resources.
     This method will release allocated resourced by `CurlWrap::initialize()` and should be called
     with each call to `initialize()` before program exits or unloads.
     @note this method internally calls `curl_global_cleanup()`.
     @see initialize()
     */
    static void cleanUp();

#ifdef _MSC_VER
#pragma region Global Functions
#endif
    /**@addtogroup Aux Functions
     @{
     */
    /**Query size in byte of given url.
     @param[in] curlWrap existed CurlWrap instance. If this value is nullptr, the created instance
        will be ignored.
     @param[in] url target URL.
     @return size in byte of given URL. If query fail (i.e. HTTP response is not 200), (size_t)(-1)
         will returned.
     */
    static long long querySize(CurlWrap* curlWrap, std::string const& url);
    /**Download given url and write to specified output stream.
     Current thread will be blocked and wait for all chunks are complete.
     @param[in] curlWrapList pointer to std::vector of already existed CurlWrap instances. If size
        of this vector is less than parallel, new CurlWrap instances will be created and inserted to
        this vector. Set this field to nullptr to skip all created CurlWrap instances.
     @param[in] url target URL with protocol and optional port.
     @param[in] out output stream. Mode of this stream will be changed to std::binary.
     @param[in] parallel max download connection, default is 1.
     @return download success or fail. */
    static bool download(
        std::vector<CurlWrap>* curlWrapList,
        std::string const& url, std::ostream& out, int parallel = 1);
    /**Download given url and write to specified local path.
     Current thread will be blocked and wait for all chunks are complete.
     @param[in] curlWrapList pointer to std::vector of already existed CurlWrap instances. If size
        of this vector is less than parallel, new CurlWrap instances will be created and inserted to
        this vector. Set this field to nullptr to skip all created CurlWrap instances.
     @param[in] url target URL with protocol and optional port.
     @param[in] path local filesystem path. If path points to an existed directory, this method will
        create new file with guessing file name. If path does not exist, the downloaded data will
        directly writes to this path.
     @param[in] parallel max download connection, default is 1.
     @return download success or fail. */
    static bool download(
        std::vector<CurlWrap>* curlWrapList,
        std::string const& url, std::string const& path, int parallel = 1);
    /**@}*/
#ifdef _MSC_VER
#pragma endregion 
#endif

    CurlWrap();
    ~CurlWrap() noexcept;
    CurlWrap(CurlWrap const& o);
    CurlWrap(CurlWrap&& o) noexcept;
    CurlWrap& operator=(CurlWrap&& o) noexcept;
    CurlWrap& operator=(CurlWrap const& o);

#ifdef _MSC_VER
#pragma region Headers and HTTP method
#endif
    /**@addtogroup Headers and HTTP method
     @{
     */
    /**Set target URL with protocol.
     Example: "http://www.google.com/", "https://www.google.com:443/".
     @param[in] url target URL with protocol.
     @return reference of current instance. */
    CurlWrap& setUrl(std::string const& url);
    /**Set HTTP request method by pre-defined enumeration value.
     @return reference of current instance. */
    CurlWrap& setMethod(Method method);
    /**Set HTTP request method by method string such as "POST", "DELETE", "OPTIONS".
     @return reference of current instance. */
    CurlWrap& setMethod(std::string const& method);

    ///**Create new HeaderBuilder instance.*/
    //HeaderBuilder newHeaderBuilder();
    
    /**Set HTTP header by HeaderBuilder.
     @param[in] headerBuilder header builder instance.
     @return reference of current instance.
     @warning parameter headerBuilder cannot be reused after applied to this method. */
    CurlWrap& setHeader(HeaderBuilder&& headerBuilder);
    /**Set HTTP header by HeaderBuilder.
     The given header instance will be copied.
     @param[in] headerBuilder header builder instance.
     @return reference of current instance.
     @warning instance of headerBuilder must be hold until `perform()` is called. */
    CurlWrap& setHeader(HeaderBuilder const& headerBuilder);
    /**Clear all custom headers. 
     @return reference of current instance. */
    CurlWrap& clearHeader();
    /**@}*/
#ifdef _MSC_VER
#pragma endregion 
#endif

#ifdef _MSC_VER
#pragma region Request Data
#endif
    /**@addtogroup Request data
     @{
     */
    ///**Set request content.
    // This method does not modify given rawData encoding. For "GET" method, this method will directly
    // return and do nothing.
    // @param[in] rawData raw data buffer. If this value is nullptr, this method will not set data.
    // @param[out] realWriteSize pointer to variable for storing real bytes that are written to
    //    underlying buffer. Set nullptr to ignore this value.
    // @param[in] size size in byte of rawData to set. If this value is less than 0, this method will
    //    treat given rawData as null-terminated and use entire data range. Default is -1.
    // @param[in] copy whether copy given rawData to internal buffer. This is useful if data will be
    //    changed right after this method.
    // @warning if set copy to true, before commit request, content of rawData must not be changed.
    // @return reference of current instance. */
    //CurlWrap& setBody(
    //    char const* rawData, size_t* realWriteSize = nullptr, size_t size = -1, bool copy = false);
    /**Set HTTP request method to POST and set content.
     This method automatically change request method to POST and act like
     `setBody(char const*, size_t)`.
     @param[in] rawData raw data buffer. If this value is nullptr, this method will not set data.
     @param[out] realWriteSize pointer to variable for storing real bytes that are written to
        underlying buffer. Set nullptr to ignore this value.
     @param[in] size size in byte of rawData to set. If this value is less than 0, this method will
        treat given rawData as null-terminated and use entire data range.
     @param[in] copy whether copy given rawData to internal buffer. This is useful if data will be
        changed right after this method.
     @warning before commit request, content of rawData must not be changed.
     @return reference of current instance. */
    CurlWrap& setPostBody(
        char const* rawData, size_t* realWriteSize = nullptr, long long size = -1, bool copy = false);

    /**Set HTTP GET method parameter.
     This method will set request method to GET and escape given key, value. Multiple call to this
     method will append given parameters.
     @note this method will disable body of this request.
     @param[in] key key of parameter.
     @param[in] value value of parameter.
     @return reference of current instance. */
    CurlWrap& addGetParameter(std::string const& key, std::string const& value);

    /**Set callback function for handling data receiving. This is useful when downloading file.
     If the received data wants directly to write to file, use
     `setWriteDataCallback(nullptr, FILE*)` to directly write data to a opened FILE*.
     
     @param[in] callback callback function. Set to null to disable.
     @param[in] customParameter custom parameter to pass to callback. If @p callback is null, this
        parameter will be cast to C-style FILE* and CURL will internally write received data here.
     @return reference of current instance. */
    CurlWrap& setWriteDataCallback(WriteDataCallback callback, void* customParameter = nullptr);
    /**Set callback function for handling header data receiving.
     If @p callback is null but @p customParameter is not null, CURL will try to invoke callback
     set by `setWriteDataCallback`. If it is null, given @p customParameter will be cast to FILE*
     and all header data will be written here.
     
     @param[in] callback callback function. Set to null to disable.
     @param[in] customParameter custom parameter to pass to callback. If @p callback is null, this
        parameter will be cast to C-style FILE* and CURL will internally write received data here.
     @return reference of current instance. */
    CurlWrap& setHeaderDataCallback(WriteDataCallback callback, void* customParameter = nullptr);
    /**Set callback function for handling data sending. This is useful for uploading file.
     If the received data wants directly to write to file, use
     `setWriteDataCallback(nullptr, FILE*)` to directly write data to a opened FILE*.
     
     @param[in] callback callback function. Set to null to disable.
     @param[in] customParameter custom parameter to pass to callback. If @p callback is null, this
        parameter will be cast to C-style FILE* and CURL will internally read data here.
     @return reference of current instance. */
    CurlWrap& setReadDataCallback(ReadDataCallback callback, void* customParameter = nullptr);

    /**@}*/
#ifdef _MSC_VER
#pragma endregion
#endif

#ifdef _MSC_VER
#pragma region Operations
#endif
    /**@addtogroup Operations
     @{
     */
    /**Perform request in blocking mode.
     @return operation is success or not. */
    CurlWrap& perform();
    /**Reset this request.
     All settings, headers, and data will be cleared.
     @note this will not change result code. */
    void reset();
    /**@}*/
#ifdef _MSC_VER
#pragma endregion 
#endif

#ifdef _MSC_VER
#pragma region Cookies
#endif
    /**@addgroup Cookies
     CURL provides internal engine for handling cookie automatically, but this feature is initially
     disabled. By calling any of the following `setCookies*` methods to activate this feature.

     Cookie string:
     Cookie string can be represented in:
       - HTTP header format: such as "Set-Cookie: aaa=bbb; ccc=ddd;"
       - Netscape format: such as "example.com<\t>FALSE<\t>/<\t>FALSE<\t>0</t>name</t>value",
            where the "\t" means a tab character. Meaning of each fields are:
                1. Host name. (example.com)
                2. Include subdomains or not. (FALSE)
                3. Path. (/)
                4. Secure or not. (FALSE)
                5. Expire time in unix epoch second. 0 means its a session cookie. (0)
                6. Name of cookie. (name)
                7. Value of cookie. (value)

     Session cookie:
     A session cookie is a normal cookie with expire time (in unix epoch) set to 0. All session
     cookies can be "expired" by calling `newCookieSession()`.
     @{
     */
    /**Read cookie and add to internal cookie engine from file.
     The cookie file will be loaded when request is committed by calling `perform()`, and loaded
     cookies from file are skipped if there's the same cookie with the same domain, path, and name.
     
     Enable this feature to let CURL automatically store and add cookies when receiving or sending
     data to server.
     If given file path is not a valid file or is empty string, no cookies will be added, but only
     the internal cookie engine will be started.
     @param[in] cookieFilePath local file stores netscape format cookies.
     @return reference of current instance. 
     @note Cookie added by `HeaderBuilder::setCookie` will directly added to request header, even if
        they have the same domain, path, and name. */
    CurlWrap& addCookiesFromFile(std::string const& cookieFilePath);
    /**Add cookie to internal engine by given http header Set-Cookie format for netscape format.
     The cookie file will be loaded immediately and will overwrite the same cookie which is locaed
     from file with the same domain, path, and name.
     @param[in] rawCookieString raw cookie string in "Set-Cookie: xxx" format or netscape format.
     @return reference of current instance.
     @note Cookie added by `HeaderBuilder::setCookie` will directly added to request header, even if
        they have the same domain, path, and name.
     @sa https://curl.haxx.se/libcurl/c/CURLOPT_COOKIELIST.html */
    CurlWrap& addCookies(std::string const& rawCookieString);
    /**Request to use new cookie session.
     @return reference of current instance. */
    CurlWrap& newCookieSession();
    /**Write current active cookies to specified file.
     @param[in] cookieFilePath target file to write.
     @return reference of current instance.
     @note if there's no active cookie, file will not be created. */
    void flushCookieToFile(std::string const& cookieFilePath);
    /**Clear all cookies in engine.*/
    void clearCookies();
    /**Reload all cookies from previous specified files.
     This will reopen files specified in `addCookiesFromFile(std::string const&)` and reload all
     cookies.
     */
    void reloadCookies();
    /**@}*/
#ifdef _MSC_VER
#pragma endregion
#endif

#ifdef _MSC_VER
#pragma region Settings
#endif
    /**@addgroup Settings
     @{
     */
    /**Set overall request timeout in millisecond. Internal default is 0.
     @param[in] ms timeout in millisecond. Set 0 means no timeout.
     @return reference of current instance. */
    CurlWrap& setTimeout(int ms);
    /**Set request connection timeout in millisecond. Internal default is 300 seconds.
     @param[in] ms timeout in millisecond. Set 0 to use internal default value 300000.
     @return reference of current instance. */
    CurlWrap& setConnectionTimeout(int ms);

    /**Set lower bound of transfer speed (bytes per second) in time range (in second). If transfer
     speed is lower than this value within specified time range, the request is considered as fail.
     @param[in] bytesPerSecond transfer speed bytes per second.
     @param[in] seconds low speed duration in seconds.
     @example
        `setCancelSpeed(1024, 10)` means the request is considered as failed if transfer speed is
        slower than 1KBytes/s during 10 seconds.
     @return reference of current instance. */
    CurlWrap& setCancelSpeed(size_t bytesPerSecond, int seconds);

    /**Set size of data in byte to be uploaded.
     For small upload data such as general RESTful request, this method may not necessary and use
     `setPostBody` with size parameter set to -1. But for large file upload, this field is necessary
     for telling remote server real data size and prevent remote server hanging for waiting
     receiving complete data block.
     @see setReadDataCallback */
    CurlWrap& setUploadSize(size_t sizeInByte);

    /**Set uploading speed limit.
     If speed of uploading data is greater or equal to this value, the transfer will pause until
     speed is less than given value.
     @param[in] bytesPerSecond uploading speed bytes per second. Set 0 to disable speed limit.
     @return reference of current instance. */
    CurlWrap& setUploadSpeedLimit(size_t bytesPerSecond);
    /**Set downloading speed limit.
     If speed of downloading data is greater or equal to this value, the transfer will pause until
     speed is less than given value.
     @param[in] bytesPerSecond download speed bytes per second. Set 0 to disable speed limit.
     @return reference of current instance. */
    CurlWrap& setDownloadSpeedLimit(size_t bytesPerSecond);
    /**Set max connection of request. Internal default value is 5.
     @param[in] connections max connection number.
     @return reference of current instance. */
    CurlWrap& setMaxConnection(long connections);

    /**Enable SSL CA verification.
     Disable SSL verification is useful for self-signed CA. Default will do SSL verification.
     @param[in] enable enable SSL verification or not. Defautl is true.
     @param[in] useSystemCA use system defined CA database. This parameter works under Windows
        build with OpenSSL.
     @return reference of current instance. */
    CurlWrap& enableSSLVerify(bool enable = true, bool useSystemCA = true);
    /**Enable automatic URL redirect. Default will NOT handle URL redirection. */
    CurlWrap& enableRedirect(bool enable = true);
    /**Set other CURL operations with integer value.
     @sa https://curl.haxx.se/libcurl/c/curl_easy_setopt.html */
    CurlWrap& setCustomOption(unsigned curlOption, long value);
    /**Set other CURL operations with string.
     @sa https://curl.haxx.se/libcurl/c/curl_easy_setopt.html */
    CurlWrap& setCustomOption(unsigned curlOption, char const* value);
    /**Set other CURL operations with unspecified type.
     @sa https://curl.haxx.se/libcurl/c/curl_easy_setopt.html */
    CurlWrap& setCustomOption(unsigned curlOption, void* value);
    /**@}*/
#ifdef _MSC_VER
#pragma endregion
#endif

#ifdef _MSC_VER
#pragma region Information
#endif
    /**@addtogroup Information
     @{
     */
    /**Get last CURL error message.*/
    [[nodiscard]]
    std::string getErrorMessage() const;
    /**Get simple bool representation of previous operation result.*/
    [[nodiscard]]
    bool isSuccess() const;
    /**Get raw CURL result code (CURLcode or CURLMcode).*/
    [[nodiscard]]
    int getErrorCode() const;

    /**Get request response code.*/
    [[nodiscard]]
    int getResponseCode() const;
    /**Get real used HTTP method.*/
    [[nodiscard]]
    std::string getHttpMethod() const;
    /**Get real used URL.*/
    [[nodiscard]]
    std::string getUrl() const;
    /**Get remote IP.
     Note the IP might be IPv4 or IPv6. */
    [[nodiscard]]
    std::string getRemoteIP() const;
    /**Get remote port.*/
    [[nodiscard]]
    int getRemotePort() const;
    /**Get local IP.
     Note the IP might be IPv4 or IPv6. */
    [[nodiscard]]
    std::string getLocalIP() const;
    /**Get local port.*/
    [[nodiscard]]
    int getLocalPort() const;
    /**Get Content-Type in response HTTP header.*/
    [[nodiscard]]
    std::string getHttpContentType() const;
    /**Get Retry-After delay second in response HTTP header.*/
    [[nodiscard]]
    long long getHttpRetryAfterTime() const;
    /**Get remote file time in unix epoch (# of seconds since Jan. 1, 1970 UTC).
     If remote server does not support this function, `static_cast<size_t>(-1)` is returned. */
    [[nodiscard]]
    std::time_t getFileTime() const;
    /**Get real used connection count.*/
    [[nodiscard]]
    size_t getUsedConnectionCount() const;
    /**Get HTTP response cookie list.*/
    [[nodiscard]]
    std::vector<std::string> getHttpCookies() const;

    /**@addtogroup Statistics
     @{
     */
    /**Get # of redirection of this request.*/
    [[nodiscard]]
    int getRedirectCount() const;
    /**Get header size in bytes.*/
    [[nodiscard]]
    size_t getHeaderBytes() const;
    /**Get total upload bytes.*/
    [[nodiscard]]
    size_t getTotalUploadBytes() const;
    /**Get total download bytes.*/
    [[nodiscard]]
    size_t getTotalDownloadBytes() const;
    /**Get Content-Length header value in HTTP response header (i.e. download content size).
     If header does not contains this field, `static_cast<size_t>(-1)` is returned. */
    [[nodiscard]]
    size_t getHttpDownloadContentLength() const;
    /**Get Content-Length header value in request header (i.e. upload content size).
     If header does not contains this field, `static_cast<size_t>(-1)` is returned. */
    [[nodiscard]]
    size_t getHttpUploadContentLength() const;
    /**Get average upload speed in bytes/second unit.*/
    [[nodiscard]]
    size_t getUploadSpeed() const;
    /**Get average download speed in bytes/second unit.*/
    [[nodiscard]]
    size_t getDownloadSpeed() const;
    /**@}*/

    /**@addtogroup Timing
     @{
     */
    /**Get total request time in microsecond includes DNS resolving, connect, and others.
     The order of time points:
        perform()
        |
        |--|    getNameLookupTime()
        |--|--|     getConnectTimePoint()
        |--|--|--|      getSSLTimePoint()
        |--|--|--|--|       getPreTransferTimePoint()
        |--|--|--|--|--|        getStartTransferTimePoint()
        |--|--|--|--|--|--|         getTotalTime()
        |--|--|--|--|--|--|         getRedirectionTime()
     */
    [[nodiscard]]
    std::chrono::microseconds getTotalTime() const;
    /**Get all time on redirection includes name resolving, connect, server processing time.
     If no redirection, this value is 0. */
    [[nodiscard]]
    std::chrono::microseconds getTotalRedirectionTime() const;
    /**Get the time spending on DNS name resolved in microsecond. */
    [[nodiscard]]
    std::chrono::microseconds getNameLookupTime() const;
    /**Get the elapsed time on network connection in microsecond. */
    [[nodiscard]]
    std::chrono::microseconds getConnectElapseTime() const;
    /**Get the elapsed time on SSL handshake in microsecond.*/
    [[nodiscard]]
    std::chrono::microseconds getSSLElapseTime() const;
    /**Get the elapsed time on client preparing data after SSL handshake and before sending the 1st
     byte in microsecond.*/
    [[nodiscard]]
    std::chrono::microseconds getPreTransferElapseTime() const;
    /**Get the elapsed time from sending 1st byte to receive 1st replied byte. */
    [[nodiscard]]
    std::chrono::microseconds getStartTransferElapseTime() const;

    /**Get the time from committing request to connection complete in microsecond.*/
    [[nodiscard]]
    std::chrono::microseconds getConnectTimePoint() const;
    /**Get the time from committing request to SSL handshake in microsecond.*/
    [[nodiscard]]
    std::chrono::microseconds getSSLTimePoint() const;
    /**Get the time from committing request to the request data begins to send in microsecond. */
    [[nodiscard]]
    std::chrono::microseconds getPreTransferTimePoint() const;
    /**Get the time from commiting request to the first byte is received from server in microsecond.
     Note that the "first byte" may not real data, but a redirection response.  */
    [[nodiscard]]
    std::chrono::microseconds getStartTransferTimePoint() const;
    /**@}*/
    /**@}*/
#ifdef _MSC_VER
#pragma endregion 
#endif

#ifdef _MSC_VER
#pragma region Debug
#endif
    /**@addtogroup Debug
     @{
     */
    /**Enable CURL debug info output.
     @param[in] callback debug output callback. Set to null to disable debug output.
     @param[in] customParameter custom parameter to pass to callback. */
    void enableDebugInfo(DebugCallback callback, void* customParameter = nullptr);
    /**@}*/
#ifdef _MSC_VER
#pragma endregion 
#endif

private:
    struct Impl;
    std::unique_ptr<Impl> impl;
};

} // namespace josh

#endif // ! _JOSH_CURL_WRAP_H_
