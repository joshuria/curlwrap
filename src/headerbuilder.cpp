#include "headerbuilder.hpp"
#include "curlwrap.hpp"
#include "impl/headerbuilderimpl.hpp"
#include <curl/curl.h>
#include <utility>
#include <sstream>
#include <cstring>
#include <string>

using namespace josh;


namespace {
// Default Keep-Alive idle time, defined by CURL
constexpr auto const DefaultKeepAliveIdleTime = 60;
// Default Keep-Alive probe time, defined by CURL
constexpr auto const DefaultKeepAliveProbeTime = 60;
}


/*************************************************************/
// HeaderBuilder::Impl
/*************************************************************/
//HeaderBuilder::Impl::Impl(CurlWrap* wrap)
  //: wrap(wrap), headerList(nullptr),
HeaderBuilder::Impl::Impl()
  : headerList(nullptr),
    isKeepAlive(false),
    keepAliveIdleTime(DefaultKeepAliveIdleTime), keepAliveProbeTime(DefaultKeepAliveProbeTime) {}


HeaderBuilder::Impl::~Impl() noexcept {
    curl_slist_free_all(headerList);
    headerList = nullptr;
}


//HeaderBuilder::Impl::Impl(Impl const& o)
//  : headerList(nullptr), isKeepAlive(o.isKeepAlive), keepAliveIdleTime(o.keepAliveIdleTime),
//    keepAliveProbeTime(o.keepAliveProbeTime)
//{
//    // Deep copy
//    for (auto* list = o.headerList; list; list = list->next)
//        headerList = curl_slist_append(headerList, list->data);
//}


HeaderBuilder::Impl& HeaderBuilder::Impl::operator=(Impl const& o) {
    if (&o == this) return *this;

    curl_slist_free_all(headerList);
    isKeepAlive = o.isKeepAlive;
    keepAliveIdleTime = o.keepAliveIdleTime;
    keepAliveProbeTime = o.keepAliveProbeTime;
    range = o.range;
    userAgent = o.userAgent;
    referer = o.referer;
    acceptEncoding = o.acceptEncoding;
    cookie = o.cookie;
    // Deep copy
    for (auto* list = o.headerList; list; list = list->next)
        headerList = curl_slist_append(headerList, list->data);
    return *this;
}


HeaderBuilder::Impl& HeaderBuilder::Impl::operator=(Impl&& o) noexcept {
    if (&o == this) return *this;

    //wrap = o.wrap;
    curl_slist_free_all(headerList);
    headerList = o.headerList;
    o.headerList = nullptr;
    isKeepAlive = o.isKeepAlive;
    keepAliveIdleTime = o.keepAliveIdleTime;
    keepAliveProbeTime = o.keepAliveProbeTime;
    range = std::move(o.range);
    userAgent = std::move(o.userAgent);
    referer = std::move(o.referer);
    acceptEncoding = std::move(o.acceptEncoding);
    cookie = std::move(o.cookie);
    return *this;
}


/*************************************************************/
// HeaderBuilder
/*************************************************************/
//HeaderBuilder::HeaderBuilder(void* instance): impl(new Impl(static_cast<CurlWrap*>(instance))) {}
HeaderBuilder::HeaderBuilder(): impl(new Impl()) {}


HeaderBuilder::~HeaderBuilder() noexcept = default;


//HeaderBuilder::HeaderBuilder(HeaderBuilder const& o): impl(new Impl(*o.impl)) { }


HeaderBuilder& HeaderBuilder::operator=(HeaderBuilder const& o) {
    *impl = *o.impl;
    return *this;
}


HeaderBuilder::HeaderBuilder(HeaderBuilder&& o) noexcept: impl(std::move(o.impl)) {}


HeaderBuilder& HeaderBuilder::operator=(HeaderBuilder&& o) noexcept {
    if (o.impl.get() == impl.get())
        impl = std::move(o.impl);
    else 
        *impl = std::move(*o.impl);
    return *this;
}


//bool HeaderBuilder::apply(bool copy) {
//    auto* curl = impl->wrap->impl->curl;
//    // Apply CURL special applied functions
//    if (impl->isKeepAlive) {
//        curl_easy_setopt(curl, CURLOPT_TCP_KEEPALIVE, 1);
//        curl_easy_setopt(curl, CURLOPT_TCP_KEEPIDLE, impl->keepAliveIdleTime);
//        curl_easy_setopt(curl, CURLOPT_TCP_KEEPINTVL, impl->keepAliveProbeTime);
//    }
//    else 
//        curl_easy_setopt(curl, CURLOPT_TCP_KEEPALIVE, 0);
//
//    curl_easy_setopt(curl, CURLOPT_USERAGENT,
//        impl->userAgent.empty() ? nullptr: impl->userAgent.c_str());
//    curl_easy_setopt(curl, CURLOPT_REFERER,
//        impl->referer.empty() ? nullptr: impl->referer.c_str());
//    curl_easy_setopt(curl, CURLOPT_COOKIE,
//        impl->cookie.empty() ? nullptr: impl->cookie.c_str());
//    curl_easy_setopt(curl, CURLOPT_ACCEPT_ENCODING,
//        impl->acceptEncoding.empty() ? nullptr: impl->acceptEncoding.c_str());
//
//    // Set headers
//    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, impl->headerList);
//
//    if (copy)
//        impl->wrap->impl->header = *this;
//    else
//        impl->wrap->impl->header = std::move(*this);
//    return true;
//}


HeaderBuilder& HeaderBuilder::add(std::string const& key, std::string const& value) {
    if (value != ";")
        impl->headerList = curl_slist_append(impl->headerList, (key + ": " + value).c_str());
    else 
        impl->headerList = curl_slist_append(impl->headerList, (key + ";").c_str());
    return *this;
}


HeaderBuilder& HeaderBuilder::setCookie(std::string const& value) {
    // Redirect to CURLOPT_COOKIE
    impl->cookie = value;
    return *this;
}


HeaderBuilder& HeaderBuilder::setRange(long long start, long long end) {
    impl->range = std::to_string(start) + "-" + std::to_string(end);
    return *this;
}


HeaderBuilder& HeaderBuilder::setRange(std::vector<std::pair<long long, long long>> const& rangeList) {
    if (rangeList.empty()) {
        impl->range = "";
        return *this;
    }

    std::ostringstream sout;
    sout << rangeList[0].first << "-" << rangeList[0].second;
    for (auto i = 1; i < rangeList.size(); ++i)
        sout << ", " << rangeList[i].first << "-" << rangeList[i].second;
    impl->range = sout.str();
    return *this;
}


HeaderBuilder& HeaderBuilder::setRange(std::string const& rawRangeString) {
    impl->range = rawRangeString;
    return *this;
}


HeaderBuilder& HeaderBuilder::setCookie(std::vector<std::string> const& cookies) {
    std::ostringstream sout;
    for (auto const& cookie : cookies)
        sout << cookie << "; ";
    return setCookie(sout.str());
}


HeaderBuilder& HeaderBuilder::setCookie(std::vector<std::pair<std::string, std::string>> const& cookies) {
    std::ostringstream sout;
    for (auto const& cookie : cookies)
        sout << cookie.first << "=" << cookie.second << "; ";
    return setCookie(sout.str());
}


HeaderBuilder& HeaderBuilder::setContentType(std::string const& value) {
    return add("Content-Type", value);
}


HeaderBuilder& HeaderBuilder::setReferer(std::string const& value) {
    // Redirect to CURLOPT_REFERER
    impl->referer = value;
    return *this;
}


HeaderBuilder& HeaderBuilder::setUserAgent(std::string const& value) {
    // Redirect to CURLOPT_USERAGENT
    impl->userAgent = value;
    return *this;
}


HeaderBuilder& HeaderBuilder::setKeepAlive(bool enabled, int idleSecond, int probeSecond) {
    static constexpr char const KeepAliveHeader[] = "Connection: keep-alive";

    impl->isKeepAlive = enabled;
    if (enabled) {
        impl->headerList = curl_slist_append(impl->headerList, KeepAliveHeader);
        impl->keepAliveIdleTime = idleSecond;
        impl->keepAliveProbeTime = probeSecond;
    }
    else {
        // Remove "Connection: keep-alive" from header list
        char fakeData[] = "";
        curl_slist head{ fakeData, impl->headerList };
        auto* list = &head;
        for (; list->next; list = list->next) {
            if (std::strncmp(list->next->data, KeepAliveHeader, sizeof(KeepAliveHeader)) == 0)
                break;
        }
        if (list->next) {
            auto* detachNode = list->next;
            list->next = detachNode->next;
            detachNode->next = nullptr;
            curl_slist_free_all(detachNode);
        }
    }
    return *this;
}


HeaderBuilder& HeaderBuilder::setCacheControl(std::string const& value) {
    return add("Cache-Control", value);
}


HeaderBuilder& HeaderBuilder::setCacheExpire(std::string const& value) {
    return add("Expires", value);
}


HeaderBuilder& HeaderBuilder::setCacheExpire(std::time_t timePoint) {
    char buffer[32] = { 0 };
    std::tm tm = {0, 0, 0, 0, 0, 0, 0, 0, 0};
#ifdef _MSC_VER
    gmtime_s(&tm, &timePoint);
    asctime_s(buffer, &tm);
#else
    std::gmtime_s(&timePoint, &tm);
    std::asctime_s(buffer, sizeof(buffer), tm);
#endif
    return setCacheExpire(buffer);
}


HeaderBuilder& HeaderBuilder::setAcceptMimeType(std::string const& value) {
    return add("Accept", value);
}


HeaderBuilder& HeaderBuilder::setAcceptLanguage(std::string const& value) {
    return add("Accept-Language", value);
}


HeaderBuilder& HeaderBuilder::setAcceptEncoding(std::string const& value) {
    // Redirect to CURLOPT_ACCEPT_ENCODING
    impl->acceptEncoding = value;
    return *this;
}

