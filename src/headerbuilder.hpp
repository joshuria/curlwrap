#pragma once
#ifndef _JOSH_CURL_WRAP_HEADER_BUILDER_H_
#define _JOSH_CURL_WRAP_HEADER_BUILDER_H_

#include <memory>
#include <string>
#include <ctime>
#include <vector>


namespace josh {

class CurlWrap;

/**Builder for creating request headers.
 @warning NOT thread-safe. Do not set value concurrently. */
class HeaderBuilder {
    friend class CurlWrap;
public:
    explicit HeaderBuilder();
    ~HeaderBuilder() noexcept;
    //**Copy settings from others, the applied CurlWrap instance will NOT be copied.*/
    HeaderBuilder(HeaderBuilder const& o) = delete;
    HeaderBuilder(HeaderBuilder&& o) noexcept;
    /**Copy settings from others, the applied CurlWrap instance will NOT be copied.*/
    HeaderBuilder& operator=(HeaderBuilder const&);
    HeaderBuilder& operator=(HeaderBuilder&& o) noexcept;

    ///**Apply header settings to CurlWrap instance.
    // @param[in] copy specify copy header settings or not. Set to false if the settings will be reuse
    //    by other CurlWrap instances. Default is false.
    // @return setting success or fail. */
    //bool apply(bool copy = false);

    /**Add custom header and value.
     To disable (remove) internal default header, use: `add("header-name", "")`.
     To set custom header without value, use: `add("custom-header-name", ";")`.  */
    HeaderBuilder& add(std::string const& key, std::string const& value);

    /**Set extra Cookie string.
     If cookie engine is enabled and multiple cookies with the same name existed in both engine and
     parameter of this method, all these cookies will be set.
     @param[in] value cookie string. Example: "AAA=bbb; CCC=ddd;".
     @note only the latest time takes effect if multiple cookie setting methods are called. */
    HeaderBuilder& setCookie(std::string const& value);
    /**Set extra Cookie string by list of cookies.
     If cookie engine is enabled and multiple cookies with the same name existed in both engine and
     parameter of this method, all these cookies will be set.
     @param[in] cookies list of cookie string. Example: ["AAA=bbb", "CCC=ddd"]
     @note only the latest time takes effect if multiple cookie setting methods are called. */
    HeaderBuilder& setCookie(std::vector<std::string> const& cookies);
    /**Set extra Cookie string by list of cookies.
     If cookie engine is enabled and multiple cookies with the same name existed in both engine and
     parameter of this method, all these cookies will be set.
     @param[in] cookies list of cookie name, value pair. Example: [("AAA", "bbb"), ("CCC", "ddd")]
     @note only the latest time takes effect if multiple cookie setting methods are called. */
    HeaderBuilder& setCookie(std::vector<std::pair<std::string, std::string>> const& cookies);
    /**Set request data Range in format "Range: <start>-<end>".
     Format see: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Range.  */
    HeaderBuilder& setRange(long long start, long long end);
    /**Set request data Range in format "Range: <start0>-<end0>,<start1>-<end1>,...".
     Format see: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Range.  */
    HeaderBuilder& setRange(std::vector<std::pair<long long, long long>> const& rangeList);
    /**Set request data Range in format "Range: <rawRangeString>".
     Format see: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Range.  */
    HeaderBuilder& setRange(std::string const& rawRangeString);

    /**Set Content-Type header.*/
    HeaderBuilder& setContentType(std::string const& value);
    /**Set Referer string.
     @note given value can be directly modified later. */
    HeaderBuilder& setReferer(std::string const& value);
    /**Set User-Agent string.
     @note given value can be directly modified later. */
    HeaderBuilder& setUserAgent(std::string const& value);
    /**Set TCP keep alive.
     Even though this will set "Connection: keep-alive", this internally this will use TCP keep
     alive probe. */
    HeaderBuilder& setKeepAlive(bool enabled, int idleSecond = 60, int probeSecond = 60);

    /**Set Cache-Control header.
     Example: "max-age=0" or "no-cache" to disable cache. */
    HeaderBuilder& setCacheControl(std::string const& value);
    /**Set Expire header.
     Example: "Wed, 21 Oct 2015 07:28:00 GMT"
     Date format see https://tools.ietf.org/html/rfc7231#section-7.1.1.1
     */
    HeaderBuilder& setCacheExpire(std::string const& value);
    /**Set Expire header by given time point.*/
    HeaderBuilder& setCacheExpire(std::time_t timePoint);

    /**Set Accept header.*/
    HeaderBuilder& setAcceptMimeType(std::string const& value);
    /**Set Accept-Language header.*/
    HeaderBuilder& setAcceptLanguage(std::string const& value);
    /**Set Accept-Encoding and automatically decode if receive supported Content-Encoding header
     later.
     This field is generally for compression. Note that if server replies compressed data, the
     Content-Length field may contains wrong value or -1. */
    HeaderBuilder& setAcceptEncoding(std::string const& value);

private:
    struct Impl;
    std::unique_ptr<Impl> impl;

}; // ! class HeaderBuilder

} // namespace josh

#endif // ! _JOSH_CURL_WRAP_HEADER_BUILDER_H_
