#pragma once
#ifndef _JOSH_CURL_MULTI_WRAP_IMPL_H_
#define _JOSH_CURL_MULTI_WRAP_IMPL_H_

#include "../curlmultiwrap.hpp"
#include <curl/curl.h>
#include <map>
#include <string>


namespace josh {

struct CurlMultiWrap::Impl {
    CURLM* curl = nullptr;
};

} // namespace josh

#endif // ! _JOSH_CURL_MULTI_WRAP_IMPL_H_

