#pragma once
#ifndef _JOSH_CURL_WRAP_IMPL_H_
#define _JOSH_CURL_WRAP_IMPL_H_

#include "../curlwrap.hpp"
#include "../headerbuilder.hpp"
#include <curl/curl.h>
#include <map>
#include <string>


namespace josh {

struct CurlWrap::Impl {
    // CURL handle, initialize in CurlWrap constructor for supporting duplication
    CURL* curl = nullptr;
    //! GET parameters (encoded)
    std::map<std::string, std::string> getParameters;
    //! HTTP header
    HeaderBuilder header;

    //! Error message. Empty string means no error.
    std::string msg = "";
    //! Simple bool specifies previous operation is success or fail.
    bool result = false;
    //! Raw CURL result code. (CURLcode type)
    int rawCode;

    ~Impl() noexcept {
        curl_easy_cleanup(curl);
        curl = nullptr;
    }

    void setResult(CURLcode code) {
        result = code == CURLE_OK;
        rawCode = code;
        msg = curl_easy_strerror(code);
    }

    void setResult(CURLMcode code) {
        result = code == CURLM_OK;
        rawCode = code;
        msg = curl_multi_strerror(code);
    }
};

} // namespace josh

#endif // ! _JOSH_CURL_WRAP_IMPL_H_

