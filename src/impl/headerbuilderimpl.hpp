#pragma once
#ifndef _JOSH_CURL_WRAP_HEADER_BUILDER_IMPL_H_
#define _JOSH_CURL_WRAP_HEADER_BUILDER_IMPL_H_

#include "../headerbuilder.hpp"
#include <curl/curl.h>


namespace josh {

class CurlWrap;

struct HeaderBuilder::Impl {
    //CurlWrap* wrap;
    curl_slist* headerList;

    bool isKeepAlive;
    int keepAliveIdleTime;
    int keepAliveProbeTime;
    std::string userAgent;
    std::string referer;
    std::string cookie;
    std::string acceptEncoding;
    // Request data range: "X-Y,A-B" format.
    std::string range;

    //explicit Impl(CurlWrap* curl);
    Impl();
    ~Impl() noexcept;
    Impl(Impl const& o) = delete;
    Impl(Impl&&) noexcept = default;
    Impl& operator=(Impl const& o);
    Impl& operator=(Impl&& o) noexcept;
};

} // namespace josh

#endif // ! _JOSH_CURL_WRAP_HEADER_BUILDER_IMPL_H_

