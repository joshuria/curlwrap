#include <iostream>
#include <string>
#include <cstdio>
#include <sstream>
#include <fstream>
#include <chrono>
//#include <curl/curl.h>
#include "curlwrap.hpp"
#include "headerbuilder.hpp"
#include <curl/curl.h>

static constexpr char const* URL = R"()";
//static constexpr char const* URL = R"()";
//static constexpr char const* URL = R"()";
static constexpr char const* Payload = R"()";
//static constexpr char const* FormDataName = R"(Token)";
//static constexpr char const* FormDataValue = R"()";


namespace {
    void debugOutput(void*, josh::CurlWrap::DebugInfoType type, char const* data, size_t size, void* param) {
        auto const writer = [&](char const* header, bool showContent = true, bool newLine = false) {
            std::fprintf(stderr, "[%13s] (%4lld bytes): ", header, size);
            auto const totalSize = static_cast<josh::CurlWrap*>(param)->getHttpDownloadContentLength();
            if (showContent)
                std::fwrite(data, sizeof(char), size, stderr);
            else
                std::fprintf(stderr, "<Binary Content>");
            if (newLine)
                std::fprintf(stderr, "\n");
            std::fflush(stderr);
        };
        switch(type) {
        case josh::CurlWrap::Text:
            writer("Info", true, false);
            break;
        case josh::CurlWrap::HeaderIn:
            writer("Header In");
            break;
        case josh::CurlWrap::HeaderOut:
            writer("Header Out");
            break;
        case josh::CurlWrap::DataIn:
            //writer("Data In", false, true);
            break;
        case josh::CurlWrap::DataOut:
            //writer("Data Out", true, true);
            break;
        case josh::CurlWrap::SSLIn:
            //writer("SSL In", false, true);
            break;
        case josh::CurlWrap::SSLOut:
            //writer("SSL Out", false, true);
            break;
        }
    } // ! debugOutput()


    size_t writeCallback(char const* data, size_t, size_t size, void* param) {
        auto* stream = static_cast<std::ostream*>(param);
        stream->write(data, size);
        return size;
    }

    void dumpCurl(josh::CurlWrap& curl, int index) {
        std::cout << "===========  " << index << "  =============\n";
        std::printf("Url: %s\n", curl.getUrl().c_str());
        std::printf("Remote IP: %s:%d\n", curl.getRemoteIP().c_str(), curl.getRemotePort());
        std::printf("Local  IP: %s:%d\n", curl.getLocalIP().c_str(), curl.getLocalPort());
        std::printf("Response Code: %d\n", curl.getResponseCode());
        std::printf("Method: %s\n", curl.getHttpMethod().c_str());
        std::printf("Content Type: %s\n", curl.getHttpContentType().c_str());
        std::printf("Retry After: %llds\n", curl.getHttpRetryAfterTime());
        std::cout << "\n";

        std::cout << "  Cookies...\n";
        std::cout << "===========================\n";
        for (auto const& c: curl.getHttpCookies()) 
            std::printf("  %s\n", c.c_str());
        std::cout << "\n";


        std::cout << "  Statistics...\n";
        std::cout << "===========================\n";
        auto const castToSecond = [](std::chrono::microseconds const& micro) {
            return std::chrono::duration<double>(micro).count();
        };
        std::cout << " ++ Time ++\n";
        std::printf("Total    time: %6.3lfs\n", castToSecond(curl.getTotalTime()));
        std::printf("DNS      time: %6.3lfs\n", castToSecond(curl.getNameLookupTime()));
        std::printf("Connect  time: %6.3lfs\n", castToSecond(curl.getConnectElapseTime()));
        std::printf("SSL      time: %6.3lfs\n", castToSecond(curl.getSSLElapseTime()));
        std::printf("PreTrans time: %6.3lfs\n", castToSecond(curl.getPreTransferElapseTime()));
        std::printf("Start    time: %6.3lfs\n", castToSecond(curl.getStartTransferElapseTime()));
        std::printf("Redirect time: %6.3lfs\n", castToSecond(curl.getTotalRedirectionTime()));
        std::cout << "\n";

        std::cout << " ++ Speed ++\n";
        std::printf("Total   Upload: %6.3lf KBytes (%6.3lf KBytes/s)\n",
            curl.getTotalUploadBytes() * .001, curl.getUploadSpeed() * .001);
        std::printf("Total Download: %6.3lf KBytes (%6.3lf KBytes/s)\n",
            curl.getTotalDownloadBytes() * .001, curl.getDownloadSpeed() * .001);
        std::cout << "\n";

        std::cout << " ++ Others ++\n";
        std::printf("Total HTTP Upload  : %6.3lf KBytes\n", curl.getHttpUploadContentLength() * .001);
        std::printf("Total HTTP Download: %6.3lf KBytes\n", curl.getHttpDownloadContentLength() * .001);
        std::printf("Total Redirect     : %d times\n", curl.getRedirectCount());
        std::printf("Total Used Connection: %lld\n", curl.getUsedConnectionCount());
        std::cout << "\n";
    }
}


int main() {
    std::cout << "Initialize CURL... " << std::endl;
    josh::CurlWrap::initialize();

    auto const startTime = std::chrono::steady_clock::now();

    //josh::CurlWrap curl;
    //curl.enableDebugInfo(debugOutput, &curl);
    //std::ostringstream writer;
    //curl.enableRedirect()
    //    .enableSSLVerify()
    //    .setWriteDataCallback(writeCallback, &writer)
    //    .addCookiesFromFile("")
    //    .setUrl(URL);

    //josh::HeaderBuilder header;
    //header.setContentType("application/json");

    //curl.setHeader(std::move(header))
    //    //.setPostBody(Payload)
    //    .perform();
    //{
    //    std::cout << "  Writer data...\n";
    //    auto oldP = writer.tellp();
    //    writer.seekp(std::ios::beg, 0);
    //    auto const size = oldP - writer.tellp();
    //    writer.seekp(std::ios::beg, oldP);
    //    std::cout << "Stream size: " << size << std::endl;
    //    if (size > 1024) {
    //        std::printf("Data size > 1 KBytes, save to file data.dat\n");
    //        std::ofstream fout("data.dat", std::ios::binary);
    //        fout.write(writer.str().c_str(), size);
    //    }
    //    else {
    //        std::printf("Data: \"%s\"\n", writer.str().c_str());
    //    }
    //}
    //dumpCurl(curl, 0);

    //josh::CurlWrap curl;
    //curl.enableDebugInfo(debugOutput, &curl);
    //curl.enableRedirect()
    //    .enableSSLVerify()
    //    .addCookiesFromFile("");
    //auto size = josh::CurlWrap::querySize(&curl, URL);
    //std::cout << "Query size: " << size << "bytes" << std::endl;
    

    constexpr auto Parallel = 5;
    std::vector<josh::CurlWrap> list(Parallel);
    for (auto& curl : list) {
        //curl.enableDebugInfo(debugOutput, &curl);
        //curl.setCustomOption(CURLOPT_MAX_RECV_SPEED_LARGE, 1024 * 1024);
    }
    josh::CurlWrap& curl = list[0];
    josh::CurlWrap::download(&list, URL, "data.dat", Parallel);

    std::cout << "  Result...\n";
    for (auto i = 0; i < Parallel; ++i)
        dumpCurl(list[i], i);

    auto const endTime = std::chrono::steady_clock::now();
    std::cout << "Total time: "
        << std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime).count() * .001
        << "s" << std::endl;

    std::cout << "Clean CURL..." << std::endl;
    josh::CurlWrap::cleanUp();

    return 0;
}

